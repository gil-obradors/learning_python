import time

def is_prime(number):
    for i in range(2, int((number ** .5 + 1))):
        if (number % i) == 0:
            break
    else:
        return True

    return False


def prime_series():
    num = 1000000
    primes_l = []
    time_a = time.time()
    for i in range(2, num+1):
        if is_prime(i) == True:
            primes_l.append(i)
            continue
    total_time = time.time() - time_a
    velocity = f"{(len(primes_l) / total_time):.2} s"
    print(f"Found {len(primes_l)} numbers in {total_time:.2}s at {velocity}")





if __name__ == "__main__":
    prime_series()
